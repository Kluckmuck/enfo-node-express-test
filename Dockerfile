FROM node:carbon-slim

COPY ./src /usr/src/
WORKDIR /usr/src/
RUN npm --no-color --no-optional install
RUN npm prune --production

CMD ["npm start"]
