const http = require('http');
const app = require('./app');
const connectToDatabase = require('./io').connect;

connectToDatabase();

app.server = http.createServer(app);
app.server.listen(8080, () => {
    console.log(`API started on port ${app.server.address().port}!`);
});
