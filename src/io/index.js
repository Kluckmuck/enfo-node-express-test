
/**
 *  NOTE: This file pretends to connect to and use a database. You don't have to modify it.
 *  That said: it might help to tweak the connection success rate for testing purposes.
 *
 **/

const CONNECTION_SUCCESS_RATE = 0.90;

let isConnected = false;
const postedArticles = [];

function connectSuccess() {
    return Math.random() < CONNECTION_SUCCESS_RATE;
}

function saveArticle() {
    postedArticles.push(data);
}

function loadArticles() {
    return postedArticles
}

const db = {
    getAllArticles: () => {
        if (isConnected) {
            const data = require('./articles.json');
            data.articles = [...data.articles, ...loadArticles()];
            return Promise.resolve(data);
        }
        return Promise.reject(new Error('No connection to database.'));
    },
    insertArticle: (data) => {
        if (isConnected) {
            saveArticle(data);
            return Promise.resolve('OK');
        }
        return Promise.reject(new Error('No connection to database.'));
    }
}

function connect() {
    return new Promise((resolve, reject) => setTimeout(() => {
        if (connectSuccess()) {
            console.log('Connection to db OK.');
            isConnected = true;
            resolve()
        } else {
            console.error('Connection to db FAILED.');
            isConnected = false;
            reject();
        }
    }, 1000));
}

module.exports = {
    db,
    connect,
}
