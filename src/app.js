
const express = require('express');
const articles = require('./api/articles');

const app = express();

// mount routers for each API resource
app.use('/articles', articles);

module.exports = app;
