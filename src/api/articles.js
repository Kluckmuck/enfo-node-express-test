
const Router = require('express').Router;
const ArticlesService = require('./services/articles');

let router = Router();

router.get('/', (req, res) => {
    ArticlesService.getAll()
        .then(data => {
            res.status(200).json(data);
        });
});

router.post('/', (req, res) => {
    // not yet implemented
    return res.sendStatus(501);
});

module.exports = router;
